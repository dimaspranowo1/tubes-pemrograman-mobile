package com.example.tubesperbom

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_search.*

class search : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val search_kota = findViewById<EditText>(R.id.search_kota)
        val search_tombol = findViewById<Button>(R.id.search_tombol)

        search_tombol.setOnClickListener({

            var kota = search_kota.text.toString()
            val intent = Intent(this@search, MainActivity::class.java)
            intent.putExtra("kota", kota)
            startActivity(intent)
        })
    }
}